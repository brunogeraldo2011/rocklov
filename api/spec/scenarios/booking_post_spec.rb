describe "POST /equipos/{equipo_id}/bookings" do
  before(:all) do
    payload = { email: "ed@gmail.com", password: "123456" }
    result = Sessions.new.login(payload)
    @ed_id = result.parsed_response["_id"]
  end
  context "solicitar locação" do
    before(:all) do
      #dado que o "joe perry" tem uma "Fender Strato" para loação

      result = Sessions.new.login({ email: "joe@gmail.com", password: "123456" })
      joe_id = result.parsed_response["_id"]

      fender = {
        thumbnail: Helpers::get_thumbnail("fender-sb.jpg"),
        name: "Fender Strato",
        category: "Cordas",
        price: 150,
      }
      MongoDB.new.remove_equipo(fender[:name], joe_id)

      result = Equipos.new.create(fender, joe_id)
      fender_id = result.parsed_response["_id"]

      #quando solicito a locação da Fender do joe

      @result = Equipos.new.booking(fender_id, @ed_id)
    end

    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end
  end
end
