describe "Post /signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "yumi", email: "yumi@gmail.com", password: "123456" }
      MongoDB.new.remove_user(payload[:email])
      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "usuario ja existe" do
    before(:all) do
      # dado que eu tenha um novo usuario
      payload = { name: "joão da silva", email: "joão@gmail.com", password: "123456" }
      MongoDB.new.remove_user(payload[:email])
      #e o email desse usuario já foi cadastrado no sistema
      Signup.new.create(payload)

      #quanod faço uma requisição para a rota /signup
      @result = Signup.new.create(payload)
    end

    it "deve retornar 409" do
      #então deve retornar 409
      expect(@result.code).to eql 409
    end

    it "deve retornar a mensagem" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end
  #nome é obrigatorio
  #email é obrigatorio
  #password é obrigatorio

  examples = [{
    title: "nome é obrigatorio",
    payload: { name: "", email: "rex@gmail.com", password: "123" },
    code: 412,
    error: "required name",
  },
              {
    title: "email é obrigatorio",
    payload: { name: "Rex", email: "", password: "123" },
    code: 412,
    error: "required email",
  },
              {
    title: "password é obrigatorio",
    payload: { name: "Rex", email: "rex@gmail.com", password: "" },
    code: 412,
    error: "required password",
  }]

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Signup.new.create(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida id do usuário" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
