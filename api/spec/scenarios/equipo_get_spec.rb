describe "GET /equipos/{equipo_id}" do
  before(:all) do
    payload = { email: "diluc@genshin.com", password: "123456" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end

  context "obter unico equipo" do
    before(:all) do
      #dado que eu tenho um novo equipamento
      @payload = {
        thumbnail: Helpers::get_thumbnail("sanfona.jpg"),
        name: "Sanfona",
        category: "outros",
        price: 499,
      }

      MongoDB.new.remove_equipo(@payload[:name], @user_id)

      # e qu tnho id desse novo equipamento
      equipo = Equipos.new.create(@payload, @user_id)
      @equipo_id = equipo.parsed_response["_id"]

      # quando faço uma requisição get por id
      @result = Equipos.new.find_by_id(@equipo_id, @user_id)
    end

    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end

    it "deve retornar o nome" do
      expect(@result.parsed_response).to include("name" => @payload[:name])
    end
  end

  context "equipo não existe" do
    before(:all) do
      @result = Equipos.new.find_by_id(MongoDB.new.get_mongo_id, @user_id)
    end

    it "deve retornar 404" do
      expect(@result.code).to eql 404
    end
  end
end

describe "GET /equipos" do
  #dado que eu tenho uma lista de equipos
  before(:all) do
    payload = { email: "juliana@gmail.com", password: "123456" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end

  context "obter lista" do
    before(:all) do
      payloads = [
        {
          thumbnail: Helpers::get_thumbnail("sanfona.jpg"),
          name: "Sanfona",
          category: "outros",
          price: 499,
        }, {
          thumbnail: Helpers::get_thumbnail("trompete.jpg"),
          name: "Trompete",
          category: "outros",
          price: 599,
        }, {
          thumbnail: Helpers::get_thumbnail("slash.jpg"),
          name: "Les Paul do Slash",
          category: "outros",
          price: 799,
        },
      ]

      payloads.each do |payload|
        MongoDB.new.remove_equipo(payload[:name], @user_id)
        Equipos.new.create(payload, @user_id)
      end

      #quando faço uma requisição get para /equipos
      @result = Equipos.new.list(@user_id)
    end

    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end

    it "deve retornar uma lista de equipos" do
      expect(@result.parsed_response).not_to be_empty
      #puts @result.parsed_response
      #puts @result.parsed_response.class
    end
  end
end
