Dado("que acesso a página de cadastro") do
  @signup_page.open
end

Quando("submeto o seguinte formulário de cadastro:") do |table|
  user = table.hashes.first
  MongoDB.new.remove_user(user[:email]) #capsulei todo o codigo do remover do mongo e passei para outra pasta que se chama mongo
  @signup_page.create(user)
end
