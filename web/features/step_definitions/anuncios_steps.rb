Dado("Login com {string} e {string}") do |email, password|
  @email = email

  #foi criado um classe que faz que seja encapsulado todo o metodo de login
  @login_page.open
  @login_page.with(email, password)
  #checkpoint para garantir que estamos no dashboard
  expect(@dash_page.on_dash?).to be true
end

Dado("que acesso o formulario de cadastro de anuncios") do
  @dash_page.goto_equipo_form
  expect(page).to have_css "#equipoForm" # é um checkpoint para garantir se foi para o lugar correto.
end

Dado("que eu tenho o seguinte equipamento:") do |table|
  @anuncio = table.rows_hash
  #log @anuncio # o @ é para fazer que essa variavel funcione em todos o blocos quando for chamado

  MongoDB.new.remove_equipo(@anuncio[:nome], @email)
end

Quando("submento o cadastro desse item") do
  @equipos_pages.create(@anuncio)
end

Então("devo ver esse item no meu Dashboard") do
  expect(@dash_page.equipo_list).to have_content @anuncio[:nome]
  expect(@dash_page.equipo_list).to have_content "R$#{@anuncio[:preco]}/dia"
end

Então("deve conter a mensagem de alerta: {string}") do |expect_alert|
  expect(@alert.dark).to have_text expect_alert #deve conter a mensagem
end

#REMOVER ANUNCIOS

Dado("que eu tenho o anuncio indesejado:") do |table|
  user_id = page.execute_script("return localStorage.getItem('user')")

  thumbnail = File.open(File.join(Dir.pwd, "features/support/fixtures/images", table.rows_hash[:thumb]), "rb")

  @equipo = {
    thumbnail: thumbnail,
    name: table.rows_hash[:nome],
    category: table.rows_hash[:categoria],
    price: table.rows_hash[:preco],
  }

  EquiposServices.new.create(@equipo, user_id)

  visit current_path
end

Quando("eu solicito a exclusão desse item") do
  @dash_page.request_removal(@equipo[:name])
  sleep 1
end

Quando("confirmo a exclusão") do
  @dash_page.confirm_removal
end

Quando("não confirmo a solicitação") do
  @dash_page.cancel_removal
end

Então("não devo ver esse item no meu Dashboard") do
  expect(
    @dash_page.has_no_equipo?(@equipo[:name])
  ).to be true
end

Então("esse item deve permanecer no meu Dashboard") do
  expect(@dash_page.equipo_list).to have_content @equipo[:name]
end
