#language:pt

Funcionalidade: Receber pedido de locação
    Sendo um anúnciante que possui equipamentos cadastrados
    Desejo receber pedidos de locação
    Para que eu possa decidir se quero aprova-los ou rejeita-los

    Cenario: Receber pedido

        Dado que meu perfil de anunciante é "joao@anunciante.com" e "123456"
            E que eu tenha o seguinte equipamento cadastrado:
            | thumb     | trompete.jpg |
            | nome      | Trompete     |
            | categoria | outros       |
            | preco     | 100          |
            E acesso o meu Dashboard
        Quando "maria@locataria.com" e "123456" solicita a locação desse equipo
        Então devo ver a seguinte mensagem
            """
            maria@locataria.com deseja alugar o equipamento: Trompete em: DATA_ATUAL
            """
            E devo ver os link: "ACEITAR" e "REJEITAR" no pedido