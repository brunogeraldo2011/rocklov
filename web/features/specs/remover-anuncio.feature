#language: pt

Funcionalidade: remover anunico
    sendo um anunciante que possui um equipamento indesejado
    quero poder remover esse anunico
    para que eu possa mante ro meu Dashboard atualizado

    Contexto: Login
        * Login com "sucrose@gmail.com" e "123456"

    @remover
    Cenario: Remover anunico
        Dado que eu tenho o anuncio indesejado:
            | thumb     | telecaster.jpg |
            | nome      | Telecaster     |
            | categoria | Cordas         |
            | preco     | 50             |
        Quando eu solicito a exclusão desse item
            E confirmo a exclusão
        Então não devo ver esse item no meu Dashboard

    @desistir
    Cenario: desistir da exclusão

        Dado que eu tenho o anuncio indesejado:
            | thumb     | conga.jpg |
            | nome      | Conga     |
            | categoria | Cordas    |
            | preco     | 100       |
        Quando eu solicito a exclusão desse item
            Mas não confirmo a solicitação
        Então esse item deve permanecer no meu Dashboard