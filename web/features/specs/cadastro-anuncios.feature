#language: pt

Funcionalidade: Cadastro de anuncios
    Sendo um usuario cadastrado na Rocklov que possui equiipamento musicais
    Quero  cadastrar meus equipamentos
    Para que eu possa disponibilizalos para locação


    Cenario: Novo equipo

        Dado Login com "bruno@gmail.com" e "123456"
            E que acesso o formulario de cadastro de anuncios
            E que eu tenho o seguinte equipamento:
            | thumb     | fender-sb.jpg |
            | nome      | Fender Strato |
            | categoria | Cordas        |
            | preco     | 200           |
        Quando submento o cadastro desse item
        Então devo ver esse item no meu Dashboard

    @sem
    Esquema do Cenario: Tentativa de cadastro de anúncios

        Dado Login com "bruno@gmail.com" e "123456"
            E que acesso o formulario de cadastro de anuncios
            E que eu tenho o seguinte equipamento:
            | thumb     | <foto>      |
            | nome      | <nome>      |
            | categoria | <categoria> |
            | preco     | <preco>     |
        Quando submento o cadastro desse item
        Então deve conter a mensagem de alerta: "<saida>"

        Exemplos:
            | foto          | nome              | categoria | preco | saida                               |
            |               | Violao de Nylon   | Cordas    | 150   | Adicione uma foto no seu anúncio!   |
            | clarinete.jpg |                   | Outros    | 250   | Informe a descrição do anúncio!     |
            | mic.jpg       | Microfone Shure   |           | 200   | Informe a categoria                 |
            | trompete.jpg  | Trompete clássico | Outros    |       | Informe o valor da diária           |
            | conga.jpg     | Gonga             | Outros    | abc   | O valor da diária deve ser numérico |
            | conga.jpg     | Gonga             | Outros    | 100a   | O valor da diária deve ser numérico|