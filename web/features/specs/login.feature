#language: pt

Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @temp
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "bruno21@gmail.com" e "123456"
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentar logar
     
    Dado que acesso a página principal
    Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
    Então vejo a mensagem de alerta: "<mensagem_output>"

    Exemplos:
    |email_input    |senha_input|mensagem_output                 |
    |bruno@gmail.com|123        |Usuário e/ou senha inválidos.   |
    |bruno@yahoo.com|123456     |Usuário e/ou senha inválidos.   |
    |bruno21&aol.com  |123456     |Oops. Informe um email válido!  |
    |               |123456     |Oops. Informe um email válido!  |
    |bruno21@gmail.com|           |Oops. Informe sua senha secreta!|
